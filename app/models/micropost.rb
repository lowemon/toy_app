class Micropost < ApplicationRecord
    belongs_to :user
    validates :content, length: {maximumm: 140},
                            presence: true
end